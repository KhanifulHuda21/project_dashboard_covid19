<?php

$curl=curl_init();
curl_setopt($curl, CURLOPT_URL , 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER ,true);
$result = curl_exec($curl);
curl_close($curl);

$result=json_decode($result,true);
// var_dump($result);die;

// ---------global-----------
$newConfirmed=$result['Global']['NewConfirmed'];
$totalConfirmed=$result['Global']['TotalConfirmed'];
$newDeaths=$result['Global']['NewDeaths'];
$totalDeaths=$result['Global']['TotalDeaths'];
$newRecovered=$result['Global']['NewRecovered'];
$totalRecovered=$result['Global']['TotalRecovered'];
//print_r($global);die;
// print_r($newConfirmed);


// -----------------------contris---------------------------
$country=$result['Countries'];
// var_dump($country);die;

$temp_con=array();
foreach($country as $key=>$value){
  $temp_con[]=$value['Country'];
}
// print_r($temp_arr);die;

$temp_code=array();
foreach($country as $key=>$value){
  $temp_code[]=$value['CountryCode'];
}
// print_r($temp_arr);die;

foreach($country as $key=>$value){
  $temp_arr[]=$value['Slug'];
}
// print_r($temp_arr);die;

$temp_newConfird=array();
foreach($country as $key=>$value){
  $temp_newConfird[]=$value['NewConfirmed'];
}
// print_r($temp_arr);die;

$temp_totalConfirmed=array();
foreach($country as $key=>$value){
  $temp_totalConfirmed[]=$value['TotalConfirmed'];
}
// print_r($temp_arr);die;


$temp_newDeth=array();
foreach($country as $key=>$value){
  $temp_newDeth[]=$value['NewDeaths'];
}
// print_r($temp_arr);die;

$temp_death=array();
foreach($country as $key=>$value){
  $temp_death[]=$value['TotalDeaths'];
}
// print_r($temp_arr);die;


$temp_newRecovered=array();
foreach($country as $key=>$value){
  $temp_newRecovered[]=$value['NewRecovered'];
}
// print_r($temp_arr);die;

$temp_arr=array();
foreach($country as $key=>$value){
  $temp_arr[]=$value['TotalRecovered'];
}
// print_r($temp_arr);die;


$temp_date=array();
foreach($country as $key=>$value){
  $temp_date[]=$value['Date'];
}
// print_r($temp_arr);die;
 
$temp_color=array();
$colors=function (){
  $r=rand(0,255);
  $g=rand(0,255);
  $b=rand(0,255);
  return "rgb(".$r.",".$g.",".$b.")";
};

foreach($country as $x=>$y){
  for($i=1; $i<=count($y); $i++){
    $temp_color[]=$colors();
  }
}

?>
<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<link rel="stylesheet" href="css/styles.css">
<link rel="shortcut icon" href="images/logos.jpg" type="image/x-icon">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<title>Coronavirus Disease (COVID-19) Dashboard</title>
</head>
<body>
<nav class="navbar navbar-light bg-dark">
  <a class="navbar-brand text-light" style="font-family: calibry;">Covid-19 Dashboard</a>
  <div class="custom-control custom-switch">
  <input type="checkbox" class="custom-control-input" onclick="myFunction()" id="customSwitch1">
  <label class="custom-control-label text-light mt-2" for="customSwitch1" style="font-size: 15px;">Dark Mode</label>
</div>
</nav>
<!-- -----------------------------------data Total -------------------------------------- -->
<div class="container mt-3">
  <div class="row justify-content-center">
  <div class="col-md-3 mt-2">
    <h1>Coronavirus </h1>
    <h6>Disease (COVID-19) Dashboard</h6>
    <p style="font-size:10px;">Data last updated : <?= json_encode($temp_date['0']);?></p>
    </div>
    <div class="col-md-3 mt-2">
    <h1 class="text-center text-warning"><?=number_format($totalConfirmed); ?></h1>
    <h6 class="text-center text-warning">Total Confirmed</h6>
    </div>
    <div class="col-md-3">
    <h1 class="text-center text-danger"><?=number_format($totalDeaths); ?></h1>
    <h6 class="text-center text-danger">Total Deaths</h6>
    </div>
    <div class="col-md-3">
    <h1 class="text-center text-success"><?=number_format($totalRecovered); ?></h1>
    <h6 class="text-center text-success">Total Recovered</h6>
    </div>
  </div>
</div>

<!-- -----------------------------------data contrys-------------------------------------- -->
<div class="container-fluid mt-3" id="bar-chart">
  <div class="row ">
    <div class="col-md-8 p-3 m-3 border rounded p-3"  style="height:500px;">
      <canvas class="mt-5" id="myChart" width="100%"></canvas>
    </div>
    <div class="col-10 col-sm-10 col-md-3 col-lg-3 mx-2 border p-3 mt-3 rounded" id="huda">
    <div class="card-header">
    <h4 class="text-center mt-2 ">List Of Countries</h4>
    </div>
      <?php foreach ($country as $value ): ?>
      <div class="mt-2 border btn btn-outline-danger" data-toggle="modal" data-target="<?php echo '#'.$value['CountryCode']; ?>" ><span><?=$value['Country']; ?></span></div>
      <div class="modal" tabindex="-1" id="<?php echo $value['CountryCode'];?>" >
        <div class="modal-dialog ">
          <div class="modal-content text-light" style="background-color:black;opacity:0.7; filter:alpha(opacity=60);">
            <div class="modal-header">
              <h4 class="modal-title "><p><?php echo $value['Slug'];?></p></h4>
            </div>
            <div class="modal-body">
              <p>Country Code : <?php echo $value['CountryCode'];?></p>
              <p>Total Deaths : <?php echo $value['TotalDeaths'];?></p>
              <p>New Confirmed : <?php echo $value['NewConfirmed'];?></p>
              <p>Total Confirmed : <?php echo $value['TotalConfirmed'];?></p>
              <p>New Deaths : <?php echo $value['NewDeaths'];?></p>
              <p>Total Deaths : <?php echo $value['TotalDeaths'];?></p>
              <p>New Recovered : <?php echo $value['NewRecovered'];?></p>
              <p>Total Recovered : <?php echo $value['TotalRecovered'];?></p>
            </div>
            <div class="modal-footer">
            </div>
          </div>
        </div>
      </div>
      <?php endforeach ;?>
    </div>
  </div>
</div>

<!-- -----------------------------------data Analysis In Numbers -------------------------------------- -->
<div class="container mt-2">
  <div class="row text-center">
    <div class="col-md-5">
    <h1 class=" mt-2">Analysis In Numbers</h1>
    </div>
    <div class="col-11 col-sm-11 col-md-2 mt-2 border rounded m-2 ml-3">
    <h6 class="text-center mt-2 ">New Deaths</h6>
    <h2 class="text-center"><?=number_format($newDeaths); ?></h2>
    </div>
    <div class="col-11 col-sm-11 col-md-2 mt-2  border rounded m-2 ml-3">
    <h6 class="text-center mt-2 ">New Confirmed</h6>
    <h2 class="text-center"><?=number_format($newConfirmed); ?></h2>
    </div>
    <div class="col-11 col-sm-11 col-md-2 mt-2  border rounded m-2 ml-3">
    <h6 class="text-center mt-2">New Recovered</h6>
    <h2 class="text-center"><?=number_format($newRecovered); ?></h2>
    </div>
  </div>
</div>

<!-- -----------------------------------data chart pie-------------------------------------- -->
<div class="container mt-1">
  <div class="row mr-3 ">
    <div class="col-md-4">
    <p class="mt-4">New Deaths</hp>
     <canvas  class="mt-4" id="newDeth"></canvas>
    </div>
    <div class="col-md-4">
    <p class="mt-4 ">New Confirmed</p>
      <canvas class="mt-4" id="newConfird"></canvas>
    </div>
    <div class="col-md-4">
    <p class="mt-4">New Recovered</hp>
      <canvas  class="mt-4" id="newRecovered" ></canvas>
    </div>
  </div>
</div>

<!-- -----------------------------------footer-------------------------------------- -->
<section class="container-fluid mt-5" style="background-color:black;opacity:0.5; filter:alpha(opacity=60);">
  <div class="row">
    <div class="col-md-5">
      <img src="images/background.png" width="450px" height="200px" alt="gambar" id="overview-image">
    </div>
    <div class="col-md-7">
    <small class="text-light" style="font-size:10px;">
     <h6 class="my-2">About this data</h6>
      Data changes rapidly
      This data is changing rapidly and may not include some cases in the reporting process.
      These statistics include both confirmed and probable cases
      The total includes both confirmed and probable cases in some locations. Cases that are still in the form are likely identified by public health officials and using criteria developed by government authorities. Some regions may not have data because they haven't published data or haven't done so recently.
      Why do I see different data from different sources?
      There are various sources that track and combine data on the corona virus. These sources update at different times and may have other ways of collecting data.
    </small>
    </div>
  </div>
</section>

<!-- -----------------------------------footer-------------------------------------- -->
<div class="container-fluid" style="background-color:black;">
  <div class="row">
    <div class="col">
         <p class="text-light text-center mt-3" style="font-size: 10px;">Khaniful Huda || &copy;2020</p>
    </div>
  </div>
</div>

<!--  jQuery, Popper.js, and Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script src="js/script.js"></script>
</body>
</html>

<!-- -----------------------------------JS-------------------------------------- -->
<script>
  var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: <?= json_encode($temp_con); ?>,
        datasets: [{
            label: '# of Votes',
            data: <?= json_encode($temp_totalConfirmed);?>,
            backgroundColor: <?= json_encode($temp_color); ?>,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

// -----------------------------------------------newConfirmd-------------------------------------------------

var ctx = document.getElementById('newConfird').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: <?= json_encode($temp_con); ?>,
        datasets: [{
            label: '# of Votes',
            data: <?= json_encode($temp_newConfird);?>,
            backgroundColor: <?= json_encode($temp_color); ?>,
            borderWidth: 1
        }]
    },
   
        options:{
            legend:{
               display:false
            }
        }
    
});

// ---------------------------------------------newDeth----------------------------------------------

var ctx = document.getElementById('newDeth').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: <?= json_encode($temp_con); ?>,
        datasets: [{
            label: '# of Votes',
            data: <?= json_encode($temp_newDeth);?>,
            backgroundColor: <?= json_encode($temp_color); ?>,
            borderWidth: 1
        }]
    },
    options:{
            legend:{
               display:false
            }
        }
});

// ---------------------------------------------newRecovered--------------------------------------------

var ctx = document.getElementById('newRecovered').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: <?= json_encode($temp_con); ?>,
        datasets: [{
            label: '# of Votes',
            data: <?= json_encode($temp_newRecovered);?>,
            backgroundColor: <?= json_encode($temp_color); ?>,
            borderWidth: 1
        }]
    },
    options:{
            legend:{
               display:false
            }
        }
});

// -----------------------------------------------Mode----------------------------------------------
function myFunction() {
   var element = document.body;
   element.classList.toggle("dark-mode");
}

</script>


